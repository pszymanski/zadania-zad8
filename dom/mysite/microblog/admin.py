from django.contrib import admin
from microblog.models import Article

class ArticleAdmin(admin.ModelAdmin):
    list_display=['pub_date','author','title']
    search_fields=['content']
    list_filter=['pub_date','tags']

admin.site.register(Article, ArticleAdmin)
