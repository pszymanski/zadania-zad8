# -*- coding: utf-8 -*-
from django.db import models
from django.forms import ModelForm
from django.forms import ValidationError
from django.contrib.auth.models import User

class Tag(models.Model):
    name=models.CharField(max_length=20, unique=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering=['name']

class TagForm(ModelForm):
    class Meta:
        model=Tag
        fields=['name']


class Article(models.Model):
    author = models.CharField(max_length=200)
    title = models.CharField(max_length=100)
    content = models.CharField(max_length=500)
    pub_date = models.DateTimeField('date published')
    tags=models.ManyToManyField(Tag, null=True, blank=True)
    edited=models.BooleanField(default=False)
    edit_date=models.DateTimeField('date edited', null=True, blank=True)
    edit_author=models.ForeignKey(User, null=True, blank=True)
    def __unicode__(self):
        return self.title

class ArticleForm(ModelForm):
    class Meta:
        model=Article
        fields=('title', 'content', 'tags')

    def clean_content(self):
        content = self.cleaned_data['content']
        num_letters = len(content)
        if num_letters < 10 or num_letters > 200:
            raise ValidationError(u"Treść nie może być krótszy niż 10 znaków i dłuższy niż 200!")
        return content
        
