# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core.urlresolvers import reverse
from models import Article
from django.utils import timezone
import datetime

def create_article(autor, tytul, tresc, days):
    """
    Tworzy artykul o dane dni wstecz
    """
    return Article.objects.create(author=autor, title=tytul, content=tresc, pub_date=timezone.now() + datetime.timedelta(days=days))

class MicroblogTest(TestCase):

    def test_index_view_with_no_articles(self):
        """
        Wyswietla odpowiednia informacje jesli nie ma zadnych wpisow
        """
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, u"Niestety nie ma jeszcze żadnych wpisów :(")
        self.assertQuerysetEqual(response.context['latest_tweet_list'], [])


    def test_index_view_with_a_past_article(self):
        """
        Artykul z przeszlosci powinny byc widoczne
        """
        create_article(autor='anonim',tytul='test',tresc='jakas testowa tresc',days=-30)
        response = self.client.get(reverse('index'))
        self.assertQuerysetEqual(response.context['latest_tweet_list'], ['<Article: test>'])


    def test_index_view_with_two_past_articles(self):
        """
        Dwa artykuly z przeszlosci powinny byc widoczne
        """
        create_article(autor='anonim',tytul='test1',tresc='pierwsza tresc',days=-10)
        create_article(autor='anonim',tytul='test2',tresc='druga tresc',days=-5)
        response = self.client.get(reverse('index'))
        self.assertQuerysetEqual(response.context['latest_tweet_list'], ['<Article: test2>', '<Article: test1>'])