from django.db import models
from django.forms import ModelForm

# Create your models here.
class Article(models.Model):
    author = models.CharField(max_length=200)
    title = models.CharField(max_length=100)
    content = models.CharField(max_length=500)
    pub_date = models.DateTimeField('date published')
    def __unicode__(self):
        return self.title

class ArticleForm(ModelForm):
    class Meta:
        model=Article
        fields=('title', 'content')

        
