# -*- coding: utf-8 -*-
# Create your views here.
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from models import Article
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


def index(request):
    latest_tweet_list = Article.objects.all().order_by('-pub_date')
    context = {'latest_tweet_list': latest_tweet_list}
    return render(request, 'microblog/stub.html', context)


def form(request):
    if request.user.is_authenticated():
        return render(request, 'microblog/dodajwpisDF.html')
    else:
        info = u"Zaloguj się, aby dodać wpis!"
        return render(request, 'microblog/loginPage.html', {'info':info})


def dodajwpis(request):
    if request.user.is_authenticated():
        autor = request.user
        tytul = request.POST['article_title']
        tresc = request.POST['article_content']
        article = Article(author=autor, title=tytul, content=tresc, pub_date=timezone.now())
        article.save()
        return HttpResponseRedirect(reverse('index'))
    else:
        info = u"Zaloguj się, aby dodać wpis!"
        return render(request, 'microblog/loginPage.html', {'info':info})


def zaloguj(request):
    if not request.user.is_authenticated():
        return render(request, 'microblog/loginPage.html')
    else:
        informacja = u"Jesteś już zalogowany"
        return render(request, 'microblog/authinfo.html', {'authinfo':informacja})


def logowanie(request):
    if not request.user.is_authenticated():
        username = request.POST['login']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                informacja = u"Pomylśnie zalogowano się!"
                return render(request, 'microblog/authinfo.html', {'authinfo':informacja})
            else:
                # Return a 'disabled account' error message
                info = 'Konto nieaktywne'
                return render(request, 'microblog/loginPage.html', {'info':info})
        else:
            info2 = u"Niewłaściwa nazwa użytkownika lub hasło!"
            return render(request, 'microblog/loginPage.html', {'info':info2})
    else:
        informacja = u"Jesteś już zalogowany"
        return render(request, 'microblog/authinfo.html', {'authinfo':informacja})


def wyloguj(request):
    if request.user.is_authenticated():
        logout(request)
        # Redirect to a success page.
        informacja = u"Pomylśnie wylogowano się!"
        return render(request, 'microblog/authinfo.html', {'authinfo':informacja})
    else:
        info = u"Zaloguj się!"
        return render(request, 'microblog/loginPage.html', {'info':info})


def mojeWpisy(request):
    if request.user.is_authenticated():
        list = Article.objects.filter(author__exact=request.user).order_by('-pub_date')
        context = {'my_articles': list}
        return render(request, 'microblog/mojeWpisy.html', context)
    else:
        info = u"Zaloguj się, aby zobaczyć swoje wpisy!"
        return render(request, 'microblog/loginPage.html', {'info':info})


def rejestracja(request):
    if not request.user.is_authenticated():
        return render(request, 'microblog/rejestracja.html')
    else:
        informacja = u"Jesteś już zarejestrowany"
        return render(request, 'microblog/authinfo.html', {'authinfo':informacja})


def zarejestruj(request):
    if not request.user.is_authenticated():
        nazwa = request.POST['user_name']
        haslo = request.POST['user_password']
        if (nazwa != "") and (haslo != ""):
            uzytkownik = User.objects.create_user(nazwa, '',haslo)
            uzytkownik.save()
            informacja = u"Gralulacje. Zarejestrowałeś się. Teraz możesz się zalogować!"
            return render(request, 'microblog/authinfo.html', {'authinfo':informacja})
        else:
            informacja = u"Login lub hasło nie mogą być puste!"
            return render(request, 'microblog/rejestracja.html', {'register_info':informacja})
    else:
        informacja = u"Jesteś już zarejestrowany"
        return render(request, 'microblog/authinfo.html', {'authinfo':informacja})
