from django.conf.urls import patterns, url, include
import views
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^dodaj$', views.form, name='n_dodajform'),
    url(r'^dodajwpis$', views.dodajwpis, name='n_dodajwpis'),
    url(r'^login$', views.zaloguj, name='n_zaloguj'),
    url(r'^logowanie$', views.logowanie, name='n_logowanie'),
    url(r'^wylogowanie$', views.wyloguj, name='n_wyloguj'),
    url(r'^moje$', views.mojeWpisy, name='n_mojewpisy'),
    url(r'^rejestracja$', views.rejestracja, name='n_rejestracja'),
    url(r'^zarejestruj$', views.zarejestruj, name='n_zarejestruj'),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^$', TemplateView.as_view(template_name='microblog/stub.html'))
)
